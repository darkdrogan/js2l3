const textSpan = document.getElementById('test');

let text = textSpan.innerText;

text = text.replace(/(\s)(')(\w)/, '$1"$3');
text = text.replace(/(^)(')(\w)/, '$1"$3');
text = text.replace(/(\w)(')(\s)/, '$1"$3');
text = text.replace(/(\w)(')($)/, '$1"$3');

textSpan.innerText = text;

document.body.replaceChild(document.getElementById('test'), textSpan);
